<?php get_header(); ?>
	
	<div class="banner" style="background-image: url(<?php echo get_stylesheet_directory_uri()?>/assets/images/map.jpg);">
		<div class="banner__inner">
			<div class="banner__title">EL-Панели и Лампы Т8</div>
			<div class="banner__subtitle">с доставкой по всему миру</div>
		</div>
	</div>
	<div class="news-data">
		<div class="row">
			<div class="col-lg-4 col-md-6 col-sm-12">
				<div class="news-item">
					<h3 class="news-item__name news-item__name--link"><a class="news-item__link" href="#">ЕL-Панели</a></h3>
					<div class="news-item__data">
						<div class="news-item__content">
							Она же светопленка, светопанель, светобумага.
							Пластичный, тонкий и легкий материал, покрытый люминофором. Отличное решение для привлечения внимания к рекламе своей продукции. Потребляет мизерное количество энергии, устойчива к влаге и вибрации, без ультрафиолетового излучения. Из неё можно вырезать любые фигуры.
						</div>
					</div>
				</div>
				<div class="news-item">
					<h3 class="news-item__name">Светодиодная продукция</h3>
					<div class="news-item__data"><a class="news-item__subtitle" href="#">Лампы Т8</a>
						<div class="news-item__content">Экономят до 65% электроэнергии. Долговечнее и экологичнее, чем люминесцентные.</div>
					</div>
					<div class="news-item__data"><a class="news-item__subtitle" href="#">Светодиодные линейки</a>
						<div class="news-item__content">Простой способ подсветки прилавков, витрин, рекламных вывесок, и освещения помещений.</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<div class="news-item">
					<h3 class="news-item__name">Полноцветная печать и постпечатная обработка</h3>
					<div class="news-item__data"><a class="news-item__subtitle" href="#">Печать на плоских материалах</a></div>
					<div class="news-item__data"><a class="news-item__subtitle" href="#">Печать на рулонных материалах</a>
						<div class="news-item__content">На плёнке, бэклите, баннерной ткани, сетке. Печатаем на японских принтерах компании Seiko.</div>
					</div>
					<div class="news-item__data"><a class="news-item__subtitle" href="#">Фрезерная резка</a>
						<div class="news-item__content">Не оставляет обгорелую кромку, в отличие от лазерной резки. Для твердых рекламных материалов до 10 мм.</div>
					</div>
					<div class="news-item__data"><a class="news-item__subtitle" href="#">Ламинация пленкой</a>
						<div class="news-item__content">Дополнительная защита печатной продукции от внешних воздействий.</div>
					</div>
					<div class="news-item__data"><a class="news-item__subtitle" href="#">Плоттерная резка</a></div>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-12">
				<div class="news-item">
					<h3 class="news-item__name">Монтаж рекламных материалов</h3>
					<div class="news-item__data"><a class="news-item__subtitle" href="#">Монтаж плёнки</a></div>
					<div class="news-item__data"><a class="news-item__subtitle" href="#">Монтаж баннеров</a></div>
					<div class="news-item__data"><a class="news-item__subtitle" href="#">Оклейка автомобилей</a></div>
					<div class="news-item__data"><a class="news-item__subtitle" href="#">Монтаж баннеров на автомобили</a></div>
				</div>
			</div>
		</div>
	</div>
	<h2 class="h2-title">За нас говорят клиенты</h2>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-12">
			<div class="blockquote">
				<div class="blockquote__inner">
					<div class="blockquote__content">«Заказывали печать материалов для ребрендинга. Такого превосходного обслуживания я ещё нигде не встречал. Все материалы были напечатаны вовремя и в отличном качестве.»</div>
				</div>
				<div class="blockquote__author">Иван Иванов, менеджер компании «Apple»</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12">
			<div class="blockquote">
				<div class="blockquote__inner">
					<div class="blockquote__content">«Заказывали печать материалов для ребрендинга. Такого превосходного обслуживания я ещё нигде не встречал. Все материалы были напечатаны вовремя и в отличном качестве.»</div>
				</div>
				<div class="blockquote__author">Пётр Петров, менеджер компании «HP»</div>
			</div>
		</div>
	</div>
				
<?php get_footer(); ?>
