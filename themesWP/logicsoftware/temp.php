<?php
	/*
    Template Name: Temp
    */
?>

<?php get_header(); ?>

    <div class="banner" style="background-image: url(<?php echo get_stylesheet_directory_uri()?>/assets/images/map.jpg);">
        <div class="banner__inner">
            <h2 class="banner__title">
                <?php echo get_the_title() ?>
            </h2>
        </div>
    </div>

<?php get_footer(); ?>
