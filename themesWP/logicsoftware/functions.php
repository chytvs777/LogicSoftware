<?php

	add_action( 'wp_enqueue_scripts', 'theme_name_scripts' );
	function theme_name_scripts() {
		wp_enqueue_style( 'styles', get_template_directory_uri() . '/assets/css/styles.min.css' );
		wp_enqueue_script( 'script-name1', get_template_directory_uri() . '/assets/js/vendor/jquery-3.2.1.min.js', array(), '1.0.0', true );
		wp_enqueue_script( 'script-name2', get_template_directory_uri() . '/assets/js/vendor/popper.js', array(), '1.0.0', true );
		wp_enqueue_script( 'script-name3', get_template_directory_uri() . '/assets/js/vendor/bootstrap.min.js', array(), '1.0.0', true );
		wp_enqueue_script( 'script-name4', get_template_directory_uri() . '/assets/js/vendor/bootstrapValidator.js', array(), '1.0.0', true );
		wp_enqueue_script( 'script-name5', get_template_directory_uri() . '/assets/js/app.min.js', array(), '1.0.0', true );
	}

    if (function_exists('add_theme_support')) {
        add_theme_support('menus');
    }
?>