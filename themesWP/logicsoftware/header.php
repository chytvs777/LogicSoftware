<?php ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700&amp;amp;subset=cyrillic-ext" rel="stylesheet">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div class="wrapper">
		<header class="header">
			<div class="container">
				<div class="header__inner">
					<div class="header__top">
						<h1 class="title header__title"><?php echo get_the_title() ?></h1>
						<div class="header__data header-data">
							<div class="header-data__item"><a class="btn header__btn" href="#" data-toggle="modal" data-target="#feedback">Обратный звонок</a></div>
							<div class="header-data__item"><a class="phone phone--ico" href="tel:88004356214">8 (800) 435-62-14</a></div>
						</div>
						<div class="btn-menu">
							<div class="btn-menu__inner">
								<div class="btn-menu__item"></div>
								<div class="btn-menu__item"></div>
								<div class="btn-menu__item"></div>
							</div>
						</div>
					</div>
					<div class="header__nav ">
						    <?php wp_nav_menu( array(
                                'container_class' => 'nav',
                                'menu_class'     => 'nav__list',
                                'item_class'     => 'nav__list',
                            ));?>
<!--							<li class="nav__item"><a class="nav__link" href="/services"> Услуги</a></li>-->
<!--							<li class="nav__item"><a class="nav__link" href="/product">Продукт</a></li>-->
<!--							<li class="nav__item"><a class="nav__link" href="/dostavka"> Доставка и оплата</a></li>-->
<!--							<li class="nav__item"><a class="nav__link" href="/about"> О компании</a></li>-->
<!--							<li class="nav__item"><a class="nav__link" href="/contacts">Контакты</a></li>--></ul>
					</div>
				</div>
			</div>
		</header>
		<main class="main">
				<div class="container">

