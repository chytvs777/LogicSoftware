<?php ?>
		</div>
			</main>
			<footer class="footer">
				<div class="container">
					<div class="footer__inner row">
						<div class="col-lg-3 col-md-4 col-sm-12">
							<div class="footer__item">
								<div class="footer-content">©, ООО "Delivery", 2010 — 2017 <br>  Эл. почта: <a href="mailto:hello@gmail.com">hello@gmail.com </a></div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-12">
							<div class="footer__item">
								<div class="footer__content">Адрес: Минск, площадь Независимости, 143, оф. 6</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-12 offset-lg-3">
							<div class="footer__item footer__item--questions">
								<div class="footer__content">Есть вопросы?<br><a href="#"> Напишите нам</a></div>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>
		<div class="modal fade" id="feedback" tabindex="-1" role="dialog" aria-labelledby="feedback" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<div class="modal-body">
						<div class="modal-body__title">Обратный звонок</div>
						<form class="form" id="feedbackForm" method="post">
							<div class="form__inner">
								<div class="form-group form__item">
									<input class="form-control form__input" type="text" name="name" placeholder="Имя" required>
								</div>
								<div class="form-group form__item">
									<input class="form-control form__input" type="tel" name="phone" placeholder="Телефон" required>
								</div>
								<div class="form__btn-inner">
									<input class="btn form__btn" type="submit" value="Заказать">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<?php wp_footer(); ?>
		<script type="text/javascript">
			$(document).ready(function () {
			    $('#feedbackForm').bootstrapValidator({
			        fields: {
			            name: {
			                validators: {
			                    notEmpty: {
			                        message: 'Введите Имя'
			                    }
			                }
			            },
			            phone: {
			                validators: {
			                    notEmpty: {
			                        message: 'Введите телефон'
			                    }
			                }
			            },
			        }
			    })
			});
		</script>
	</body>
</html>